<?php
/**
 * Blog3
 *
 * Allows users to interact with Blog3 using its API
 *
 */ 

/**
 * Implememtation of hook_requirements()
 *
 * @param $phase
 */
function blog3_requirements($phase) {
  $requirements = array();
  $t = get_t();

  $requirements['PHP-OAuth2'] = array(
    'title' => $t('PHP OAuth2 library'),
  );

  $libraries = libraries_get_libraries();
  if (isset($libraries['PHP-OAuth2'])) {
    $requirements['PHP-OAuth2']['value'] = $t('Installed');
    $requirements['PHP-OAuth2']['severity'] = REQUIREMENT_OK;
  }
  else {
    $requirements['PHP-OAuth2']['value'] = $t('Not Installed');
    $requirements['PHP-OAuth2']['severity'] = REQUIREMENT_ERROR;
    $requirements['PHP-OAuth2']['description'] = $t('Please install the PHP OAuth2 library using Git from %url', array('%url' => 'https://github.com/adoy/PHP-OAuth2.git'));
  }

  return $requirements;
}

/**
 * Implementation of hook_autoload_info()
 */
function blog3_autoload_info() {
  $path = libraries_get_path('PHP-OAuth2');
  return array(
    'Client' => array(
      'file' => 'Client.php',
      'file path' => "$path",
    ),
    'GrantType' => array(
      'file' => 'IGrantType.php',
      'file path' => "$path/GrantType",
    ),
    'AuthorizationCode' => array(
      'file' => 'AuthorizationCode.php',
      'file path' => "$path/GrantType",
    ),
  );
}

/**
 * Implementation of hook_init()
 */
function blog3_init() {
  global $_blog3_access_token_path, $_blog3_authorization_path, $_blog3_redirect_uri;

  $_blog3_access_token_path = "/oauth2/access_token";
  $_blog3_authorization_path = "/oauth2/authorize";

  if ($_SERVER["SERVER_PORT"] == 443) {
        $protocol="https";
  } else {
        $protocol="http";
  }
  $path_array=explode("/",$_SERVER['PHP_SELF']);
  unset($path_array[sizeof($path_array)-1]);
  $_blog3_redirect_uri = $protocol . "://" . $_SERVER['HTTP_HOST'] . implode("/",$path_array) . "/blog3/oauth2";
}

/**
 * Implementation of hook_menu()
 */
function blog3_menu() {
  return array(
    'blog3/oauth2' => array(
      'title' => 'Blog3 OAuth',
      'description' => 'OAuth with Blog3',
      'page callback' => 'blog3_oauth_page',
      'page arguments' => array(),
      'access arguments' => TRUE,
    ),
  );
}

function blog3_oauth_page() {
  blog3_authenticate();
  return drupal_get_form('blog3_oauth_form');
}

function blog3_oauth_form($form, $form_state){
  global $client;

  if ($client->getAccessToken()) {
    $status_text = "<p>You are authenticated with Blog3 using OAuth</p>";
    $status = 1;
  } else {
    $status_text = "<p>You are not authenticated with Blog3</p>";
    $status = 0;
  }

  $form = array(
    'status' => array(
      '#value' => $status_text,
    ),
    'blog3_url' => array (
      '#type' => 'textfield',
      '#title' => t('Blog3 URL'),
      '#description' => t("The URL for the Blog3 instance to which you want to interact (http://blog3.example.org) "),
      '#default_value' => '',
      '#size' => 40,
      '#required' => TRUE,
    ),
    'consumer_key' => array(
      '#type' => 'textfield',
      '#title' => t('Consumer Key'),
      '#description' => t("A valid consumer key from a Blog3 instance (e.g. d79caac7f54cece649881f3088da2b35)"),
      '#default_value' => '',
      '#size' => 40,
      '#required' => TRUE,
    ),
    'consumer_secret' => array( 
      '#type' => 'textfield',
      '#title' => t('Consumer Secret'),
      '#description' => t("A valid consumer secret from a Blog3 instance (e.g. d79caac7f54cece649881f3088da2b35)"),
      '#default_value' => '',
      '#size' => 40,
      '#required' => TRUE,
    ), 
    
    'save_credentials' => array(
      '#value'  => t("Save Credentials"),
      '#type'   => 'submit',
      '#submit' => array('blog3_oauth_form_submit'),
    ),
  );

  

  if ($status) {
    $form['delete_access_token'] = array(
      '#value'  => t("Delete Access Token"),
      '#type'   => 'submit',
      '#submit' => array('blog3_oauth_form_submit'),
    );
  }
  else {
    $form['authenticate'] = array(
      '#value'  => t("Authenticate"),
      '#type'   => 'submit',
      '#submit' => array('blog3_oauth_form_submit'),
    );
  }
  return $form;
}

function blog3_oauth_form_submit($form, &$form_state) {
  global $user, $client;
  $op = $form_state['clicked_button']['#value'];
  $values = $form_state['values'];
  switch ($op) {
    case t('Save Credentials'):
      $result = db_query("SELECT * FROM blog3_oauth WHERE uid = %s", $user->uid);
      if (db_fetch_object($result)) {
        db_query("UPDATE blog3_oauth SET (consumer_key,consumer_secret,blog3_url,access_token,access_token_state) VALUES ('%s','%s','%s','','') WHERE uid = %s", $values['consumer_key'], $values['consumer_secret'], $values['blog3_url'], $user->uid);
      } else {
        db_query("INSERT INTO blog3_oauth (uid,consumer_key,consumer_secret,blog3_url) VALUES ('%s','%s','%s','%s')", $user->uid, $values['consumer_key'], $values['consumer_secret'], $values['blog3_url']);
      }
      break;
    case t('Delete Access Token'):
      $client->revokeToken();
      db_query("UPDATE blog3_oauth SET (access_token,access_token_state) VALUES ('','') WHERE uid = %s", $user->uid);
      drupal_set_message(t("Blog3 access token deleted."));
      break;
    case t("Authenticate"):
      db_query("UPDATE blog3_oauth SET (access_token,access_token_state) VALUES ('','pending') WHERE uid = %s", $user->uid);
      break;
  }
}

function blog3_authenticate() {
  global $client, $user, $_blog3_oauth_access_token_path, $_blog3_oauth_authorization_path, $_blog3_redirect_uri;

  $blog3_oauth = db_result(db_query("SELECT * FROM blog3_oauth WHERE uid = %s", $user->uid));

  $client = new OAuth2\Client($blog3_oauth['consumer_key'], $blog3_oauth['consumer_secret']);
  if (!isset($_GET['code'])) {
    if ($_GET['error']=="access_denied") {
      db_query("UPDATE blog3_oauth SET access_token = '', access_token_state='' WHERE uid = '%s'", $user->uid);
      drupal_set_message("Blog3 authentication unsuccessful","error");
    } else {
      $auth_url = $client->getAuthenticationUrl($blog3_oauth['url'].$_blog3_oauth_authorization_path, $_blog3_redirect_uri);
      header('Location: ' . $auth_url);
    }
  } else {
    if ($blog3_oauth->access_token_state == 'active'){
      $client->setAccessToken($blog3_oauth->access_token);
    } else {
      $params = array('code' => $_GET['code'], 'redirect_uri' => $__blog3_redirect_uri);
      $response = $client->getAccessToken($blog3_oauth['url'] . $_blog3_oauth_access_token_path, 'authorization_code', $params);
      parse_str($response['result'], $info);
      db_query("UPDATE blog3_oauth SET access_token = '%s', access_token_state='active' WHERE uid = '%s'", $info['access_token'], $user->uid);
      $client->setAccessToken($info['access_token']);
    }
  }
} 
